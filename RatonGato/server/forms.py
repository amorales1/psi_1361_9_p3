from django import forms
from server.models import User

class UserForm(forms.ModelForm):
    userName = forms.CharField(max_length=128, help_text="Por favor, introduce tu nombre de usuario")
    password = forms.CharField(widget=forms.PasswordInput, max_length=128, help_text="Por favor, introduce tu contrasenia")

    class Meta:
        model = User
        fields = ('userName','password',)


class LoginForm(forms.Form):
    userName = forms.CharField(max_length=128, help_text="Por favor, introduce tu nombre de usuario")
    password = forms.CharField(widget=forms.PasswordInput, max_length=128, help_text="Por favor, introduce tu contrasenia")

    class Meta:
        model = User
        fields = ('userName','password',)

class user_form():
    userName = forms.CharField(max_length=128, help_text="Por favor, introduce tu nombre de usuario")
    password = forms.CharField(widget=forms.PasswordInput, max_length=128, help_text="Por favor, introduce tu contrasenia")

    class Meta:
        model = User
        fields = ('userName','password',)	


