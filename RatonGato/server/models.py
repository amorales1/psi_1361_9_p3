from __future__ import unicode_literals
from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator

class User(models.Model):
    userName = models.CharField(max_length=128, unique=True)
    password = models.CharField(max_length=128) #!!!!!

    def __unicode__(self):
        return self.userName

class Game(models.Model):
    catUser = models.ForeignKey(User, related_name='game_catUsers')    
    mouseUser = models.ForeignKey(User,null=True)
    cat1 = models.IntegerField(default=0, validators=[MaxValueValidator(63), MinValueValidator(0)], null=False)
    cat2 = models.IntegerField(default=2, validators=[MaxValueValidator(63), MinValueValidator(0)], null=False)
    cat3 = models.IntegerField(default=4, validators=[MaxValueValidator(63), MinValueValidator(0)], null=False)
    cat4 = models.IntegerField(default=6, validators=[MaxValueValidator(63), MinValueValidator(0)], null=False)
    mouse = models.IntegerField(default=59, validators=[MaxValueValidator(63), MinValueValidator(0)], null=False)
    catTurn = models.BooleanField(default=True, null=False)

    def __unicode__(self):      #For Python 2, use __str__ on Python 3
        return self.cat1

class Move(models.Model):
    origin = models.IntegerField(validators=[MaxValueValidator(63), MinValueValidator(0)], null=False)
    target = models.IntegerField(validators=[MaxValueValidator(63), MinValueValidator(0)], null=False)
    game = models.ForeignKey(Game, related_name='Game')

    def __unicode__(self):      #For Python 2, use __str__ on Python 3
        return self.origin

class Counter(models.Model):
    counterGlobal = models.IntegerField(validators=[MinValueValidator(0)], default=0)

    def __unicode__(self):      #For Python 2, use __str__ on Python 3
        return self.counterGlobal
