from django.shortcuts import render
from django.http import HttpResponse
from server.models import User
from server.forms import UserForm
from server.forms import LoginForm
from server.models import Counter


def index(request):

    user_list = User.objects.order_by('userName')[:30]
    context_dict = {'users': user_list}

    return render(request, 'server/index.html', context_dict)



def register_user(request):


    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form = UserForm(data=request.POST)        

        # If the two forms are valid...
        if user_form.is_valid():
            # Save the user's form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            
            user.save()            

            # Update our variable to tell the template registration was successful.
            registered = True

        # Invalid form or forms - mistakes or something else?
        # Print problems to the terminal.
        # They'll also be shown to the user.
        else:
            print user_form.errors

    # Not a HTTP POST, so we render our form using two ModelForm instances.
    # These forms will be blank, ready for user input.
    else:
        user_form = UserForm()

    # Render the template depending on the context.
    return render(request,
            'server/register.html',
            {'user_form': user_form, 'registered': registered} )


def login_user(request):

	context = {}
	try:
		username = request.GET['username']
		password = request.GET['password']
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				auth_login(request, user)
			else:
				context['error'] = 'Non active user'
		else:
			context['error'] = 'Wrong username or password'
	except:
		context['error'] = ''
	
	populateContext(request, context)
	return render(request, 'server/login.html', context)

def counter(request):

	
	if 'counterSes' in request.session:
		counterSes = request.session['counterSes']
		counterSes += 1
		request.session['counterSes'] = counterSes
	else:
		request.session['counterSes'] = 1
		counterSes = request.session['counterSes']

	
	count = Counter.objects.get_or_create(id = 1)[0]
	count.counterGlobal += 1
	counterGlobal = count.counterGlobal
	count.save()

	context_dict = {'counterSes' : counterSes, 'counterGlobal' : counterGlobal}
	

	return render(request, 'server/counter.html', context_dict)


def status_board(request):

	form = LoginForm(request.POST)

	return render(request, 'server/board.html', {'form': form})


def clean_orphan_games(request):

	form = LoginForm(request.POST)

	return render(request, 'server/clean.html', {'form': form})


def delete(request):

	form = LoginForm(request.POST)

	return render(request, 'server/delete.html', {'form': form})


def create_game(request):

	form = LoginForm(request.POST)

	return render(request, 'server/game.html', {'form': form})


def join_game(request):

	form = LoginForm(request.POST)

	return render(request, 'server/join.html', {'form': form})


def logout_user(request):

	form = LoginForm(request.POST)

	return render(request, 'server/logout.html', {'form': form})


def move(request):

	form = LoginForm(request.POST)

	return render(request, 'server/move.html', {'form': form})

def nologged(request):

	form = LoginForm(request.POST)

	return render(request, 'server/nologged.html', {'form': form})


def status_turn(request):

	form = LoginForm(request.POST)

	return render(request, 'server/turn.html', {'form': form})


def populateContext(request, context):
	context['authenticated'] = request.user.is_authenticated()
	if context['authenticated'] == True:
		context['username'] = request.user.username














