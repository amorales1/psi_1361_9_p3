import os,django
os.environ.setdefault('DJANGO_SETTINGS_MODULE','RatonGato.settings')
django.setup()
from server.models import User
from server.models import Game
from server.models import Move

#primero#
id=10
userName='u10'
password='p10'
u=User.objects.filter(id=id)
if u.exists():
	u10=u[0]
else:
	u10=User(id=id, userName=userName, password=password)
	u10.save()

#segundo#
id=11
userName='u11'
password='p11'
u=User.objects.filter(id=id)
if u.exists():
	u11=u[0]
else:
	u11=User(id=id, userName=userName, password=password)
	u11.save()

#tercero#
u10=User(id=10, userName=userName,password=password)
g1=Game(id=1,catUser=u10)
g1.save()

#cuarto#
g2=Game.objects.filter(mouseUser__isnull=True)
if g2.exists():
	print("Encuentra el juego")
else:
	print("No encuentra el juego")

#quinto#
u11=User(id=11, userName=userName, password=password)
g1.mouseUser=u11
g1.save()

#sexto#
m1=Move(origin=2,target=11,game=g1)
g1.cat2=11
g1.catTurn=False
m1.save()
g1.save()

#septimo#
m2=Move(origin=59,target=52,game=g1)
g1.mouse=52
g1.catTurn=True
m2.save()
g1.save()
