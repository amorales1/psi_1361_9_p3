from django.contrib import admin
from server.models import User, Game, Move

class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'password')

class GameAdmin(admin.ModelAdmin):
    list_display = ('catUser', 'mouseUser', 'cat1', 'cat2', 'cat3', 'cat4', 'mouse', 'catTurn')

class MoveAdmin(admin.ModelAdmin):
    list_display = ('origin', 'target', Game)

admin.site.register(User, UserAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(Move, MoveAdmin)
