from django.conf.urls import patterns, url
from server import views

#EL SEGUNDO SE REFIERE AL METODO DE VIEWS, Y LOS OTROS AL NOMBRE PUESTO EN INDEX

urlpatterns = [
	url(r'^$', views.index, name='index'),
	url(r'^index/$', views.index, name='index'),
        url(r'^login_user/$', views.login_user, name='login_user'), #Este se refiere al views.py 
	url(r'^counter/$', views.counter, name='counter'),
	url(r'^status_board/$', views.status_board, name='status_board'),
	url(r'^clean_orphan_games/$', views.clean_orphan_games, name='clean_orphan_games'),
	url(r'^delete/$', views.delete, name='delete'),
	url(r'^create_game/$', views.create_game, name='create_game'),
	url(r'^join_game/$', views.join_game, name='join_game'),
	url(r'^logout_user/$', views.logout_user, name='logout_user'),
	url(r'^move/$', views.move, name='move'),
	url(r'^register_user/$', views.register_user, name='register_user'),
	url(r'^nologged/$', views.nologged, name='nologged'),
	url(r'^status_turn/$', views.status_turn, name='status_turn')
]
